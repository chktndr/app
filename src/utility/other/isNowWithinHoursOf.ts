import moment = require('moment');

/**
 * @description Determines if the current time is within a certain
 *  number of hours of another time.
 * @export
 * @param {number} hours The number of hours.
 * @param {Date} time The other time.
 * @returns True if within the range; false otherwise.
 */
export function isNowWithinHoursOf(hours: number, time: Date) {
  const then = moment(time);
  const now = moment();

  const diff = moment.duration(now.diff(then));

  return diff.asHours() < hours;
}
