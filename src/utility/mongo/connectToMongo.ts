import * as mongoose from 'mongoose';

/**
 * @description Connect to mongodb.
 * @export
 * @param {string} uri The db uri.
 * @param {mongoose.ConnectionOptions} options Connection options.
 * @returns
 */
export function connectToMongo(uri: string, options: mongoose.ConnectionOptions) {

  const retryInterval = options.reconnectInterval || 5000;

  const result = new Promise((resolve) => {
    const onError = () => {
      mongoose.connection.removeListener('open', onOpen);
      mongoose.connection.removeListener('error', onError);
      setTimeout(() => resolve(connectToMongo(uri, options)), retryInterval);
    };

    const onOpen = () => {
      mongoose.connection.removeListener('open', onOpen);
      mongoose.connection.removeListener('error', onError);
      resolve();
    };

    mongoose.connection.on('open', onOpen);
    mongoose.connection.on('error', onError);
  });

  mongoose.connect(uri, options);
  return result;
}
