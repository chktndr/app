import { Chunk, Context } from 'dustjs-linkedin';
import { isNowWithinHoursOf } from '../other';

/**
 * @description Determines if the current time is within a certain timespan of an other time.
 * @export
 * @param {Chunk} chunk ignored
 * @param {Context} context ignored
 * @param {*} bodies ignored
 * @param {*} params contains the parameters of interest
 */
export function isNowWithin(chunk: Chunk, context: Context, bodies: any, params: any) {
  if (!params.hours || !params.otherTime) {
    return false;
  }

  return isNowWithinHoursOf(params.hours, params.otherTime);
}
