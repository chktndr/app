import { NextFunction, Request, Response, Router } from 'express';
import { body, query } from 'express-validator/check';
import { sanitizeBody, sanitizeQuery } from 'express-validator/filter';
import { Types } from 'mongoose';
import { ILogger } from '../../logging';
import { RouterBase } from '../../routing';
import { IOrder, IOrderType, Order, OrderState } from '../models';

import ObjectId = Types.ObjectId;
import { IMailer } from '../../mailing';
import { User } from '../../users';
import { isNowWithinHoursOf } from '../../utility';

/**
 * @description Router/controller which handles user interactions with orders.
 * @export
 * @class OrderRouter
 * @extends {RouterBase}
 */
export class OrderRouter extends RouterBase {
  /**
   * @description Page size for paged data.
   * @private
   * @type {number}
   * @memberof OrderRouter
   */
  private pageSize: number;

  /**
   * @description The different available types of orders.
   * @private
   * @type {IOrderType[]}
   * @memberof OrderRouter
   */
  private orderTypes: IOrderType[];

  /**
   * @description Hours to allow messaging after order is archived.
   * @private
   * @type {number}
   * @memberof OrderRouter
   */
  private messagingTime: number;

  /**
   * @description Mailing service.
   * @private
   * @type {IMailer}
   * @memberof OrderRouter
   */
  private mailer: IMailer;

  /**
   * Creates an instance of OrderRouter.
   * @param systemState The current system state object.
   * @param config The controller's configuration.
   * @memberof OrderRouter
   */
  public constructor(
    systemState: {
      app: Router,
    },
    config: {
      orderTypes: IOrderType[]
      pageSize: number,
      logger: ILogger,
      messagingTime: number,
      mailer: IMailer,
    },
  ) {
    super(systemState, config);
    this.orderTypes = config.orderTypes;
    this.pageSize = config.pageSize;
    this.messagingTime = config.messagingTime;
    this.mailer = config.mailer;
  }

  /**
   * @description Registers the routes handled by this router.
   * @param {string} mountPoint The URI under which to mount the routes.
   * @memberof OrderRouter
   */
  public registerRoutes(mountPoint: string): void {
    const router = Router();

    router.use((req, res, next) => {
      res.locals.nav = 'orders';
      next();
    });

    router.get(
      '/',
      (req, res) => this.renderOrders(req, res),
    );

    router.get(
      '/join',
      query('id').exists().not().isEmpty(),
      sanitizeQuery('id').trim(),
      (req, res, next) => this.validateRequest(req, res, next),
      (req, res, next) => this.storeReturnTo(req, res, next),
      (req, res, next) => this.renderJoinOrder(req, res, next),
      (req, res) => this.redirectToLast(req, res),
    );

    router.post(
      '/join',
      query('id').exists().not().isEmpty(),
      sanitizeQuery('id').trim(),
      (req, res, next) => this.validateRequest(req, res, next),
      (req, res, next) => this.joinOrder(req, res, next),
      (req, res) => this.redirectToLast(req, res),
    );

    router.get(
      '/confirmLeave',
      query('id').exists().not().isEmpty(),
      sanitizeQuery('id').trim(),
      (req, res, next) => this.validateRequest(req, res, next),
      (req, res, next) => this.storeReturnTo(req, res, next),
      (req, res, next) => this.renderConfirmLeaveOrder(req, res, next),
      (req, res) => this.redirectToLast(req, res),
    );

    router.get(
      '/leave',
      query('id').exists().not().isEmpty(),
      sanitizeQuery('id').trim(),
      (req, res, next) => this.validateRequest(req, res, next),
      (req, res, next) => this.leaveOrder(req, res, next),
      (req, res) => this.redirectToLast(req, res),
    );

    router.get(
      '/message',
      query('id').exists().not().isEmpty(),
      sanitizeQuery('id').trim(),
      (req, res, next) => this.validateRequest(req, res, next),
      (req, res, next) => this.storeReturnTo(req, res, next),
      (req, res, next) => this.renderMessageOrder(req, res, next),
      (req, res) => this.redirectToLast(req, res),
    );

    router.post(
      '/message',
      query('id').exists().not().isEmpty(),
      body('message').exists().not().isEmpty(),
      sanitizeQuery('id').trim(),
      sanitizeBody('message').trim(),
      (req, res, next) => this.validateRequest(req, res, next),
      (req, res, next) => this.messageOrderParticipants(req, res, next),
      (req, res) => this.redirectToLast(req, res),
    );

    this.app.use(mountPoint, router);
  }

  /**
   * @description Renders the main list of orders.
   * @private
   * @param {Request} req Express request object.
   * @param {Response} res Express response object.
   * @memberof OrderRouter
   */
  private renderOrders(req: Request, res: Response) {

    Order.getSortedPagedOrders(
      req.query.page ? parseInt(req.query.page.toString(), 10) : 1,
      this.pageSize,
    ).then((result) => {
      res.render('orders', {
        user: req.user,
        orders: result.orders,
        page: result.page,
        pages: result.pages,
        messagingTime: this.messagingTime,
      });
    }).catch((err) => {
      this.logger.error(`Error rendering orders: ${err}`);
      res.sendStatus(500);
    });
  }

  /**
   * @description Joins the current user to the specified order.
   * @private
   * @param {Request} req Express request object.
   * @param {Response} res Express response object.
   * @param {NextFunction} next Express next function.
   * @memberof OrderRouter
   */
  private joinOrder(req: Request, res: Response, next: NextFunction) {
    Order.findOne({
      '_id': ObjectId(req.query.id.toString()),
      'state': {
        $in: [OrderState.Open, OrderState.Closing],
      },
      'userOrders.user': {
        $nin: [req.user._id],
      },
    }).populate('userOrders.user').exec().then((order) => {
      if (!order) {
        throw new Error('Order is null');
      }

      const thisOrderType = this.orderTypes.find((x) => x.name === order.location);
      if (!thisOrderType) {
        throw new Error(`Failed to find order type for order ${order._id}`);
      }

      const details: string[] = [];
      for (const orderField of thisOrderType.fields) {
        const fieldValue = req.body[orderField.name];
        if (!fieldValue) {
          throw new Error(`Value ${orderField.name} was not provided`);
        }

        details.push(`${orderField.name}: ${fieldValue}`);
      }

      order.userOrders.push({
        user: req.user,
        details,
      });

      return order.save();
    }).then(() => req.flash('success', 'Successfully joined order')).catch((err) => {
      this.logger.error(`Error joining order: ${err}`);
      req.flash('error', 'Failed to join order; it may already be closed');
    }).then(() => next());
  }

  /**
   * @description Renders the confirmation to leave order page.
   * @private
   * @param {Request} req Express request object.
   * @param {Response} res Express response object.
   * @param {NextFunction} next Express next function.
   * @memberof OrderRouter
   */
  private renderConfirmLeaveOrder(req: Request, res: Response, next: NextFunction): void {
    Order.findOne({
      _id: ObjectId(req.query.id.toString()),
      state: {
        $in: [OrderState.Open, OrderState.Closing],
      },
      userOrders: {
        $elemMatch: {
          user: ObjectId(req.user._id),
        },
      },
    }).then((order) => {
      if (!order) {
        throw new Error('Order is null');
      }
      const referrer = req.header('referrer');
      res.render('confirmLeaveOrder', {
        order,
        returnTo: referrer ? referrer : '/',
      });
    }).catch((err) => {
      this.logger.error(`Error rendering confirmLeaveOrder: ${err}`);
      req.flash('error', 'Failed to load order');
      next();
    });
  }

  /**
   * @description Renders the join order page.
   * @private
   * @param {Request} req Express request object.
   * @param {Response} res Express response object.
   * @param {NextFunction} next Express next function.
   * @memberof OrderRouter
   */
  private renderJoinOrder(req: Request, res: Response, next: NextFunction): void {
    Order.findOne({
      _id: ObjectId(req.query.id.toString()),
      state: {
        $in: [OrderState.Open, OrderState.Closing],
      },
    }).exec().then((order) => {
      if (!order) {
        throw new Error('Order is null');
      }
      res.render('joinOrder', {
        order,
        orderDefinition: this.orderTypes.find((x) => x.name === order.location),
      });
    }).catch((err) => {
      this.logger.error(`Error rendering joinOrder: ${err}`);
      req.flash('error', 'Failed to load order');
      next();
    });
  }

  /**
   * @description Sends email to the order participants
   * @private
   * @param {Request} req request object
   * @param {Response} res response object
   * @param {NextFunction} next next function
   * @memberof OrderRouter
   */
  private messageOrderParticipants(req: Request, res: Response, next: NextFunction) {
    Order.findOne({
      _id: ObjectId(req.query.id.toString()),
      state: {
        $nin: [OrderState.Cancelled],
      },
    }).populate('userOrders.user').exec().then((order) => {
      if (!order) {
        throw new Error('Order is null');
      }

      this.checkOrderMessageTime(order);

      return this.mailer.sendMail(
        'orderMessage',
        {
          order,
          message: req.body.message,
          sender: req.user,
        },
        order.userOrders.map((x) => x.user.email),
        User.find({
          roles: 'order-creator',
        }),
      );
    })
      .then(() => req.flash('success', 'Successfully sent message'))
      .catch(((err) => {
        this.logger.error(`Error sending order message: ${err}`);
        req.flash('error', 'Failed to send message to order participants');
      })).then(() => next());
  }

  /**
   * @description Renders the message order page.
   * @private
   * @param {Request} req Express request object.
   * @param {Response} res Express response object.
   * @param {NextFunction} next Express next function.
   * @memberof OrderRouter
   */
  private renderMessageOrder(req: Request, res: Response, next: NextFunction): void {
    Order.findOne({
      _id: ObjectId(req.query.id.toString()),
      state: {
        $nin: [OrderState.Cancelled],
      },
    }).populate('userOrders.user').exec().then((order) => {
      if (!order) {
        throw new Error('Order is null');
      }

      this.checkOrderMessageTime(order);

      res.render('messageOrder', {
        order,
      });
    }).catch((err) => {
      this.logger.error(`Error rendering messageOrder: ${err}`);
      req.flash('error', 'Failed to load order');
      next();
    });
  }

  /**
   * @description Removes the requesting user from the specified order.
   * @private
   * @param {Request} req Express request object.
   * @param {Response} res Express response object.
   * @param {NextFunction} next Express next function.
   * @memberof OrderRouter
   */
  private leaveOrder(req: Request, res: Response, next: NextFunction): void {
    Order.findOneAndUpdate(
      {
        _id: ObjectId(req.query.id.toString()),
        state: {
          $in: [OrderState.Open, OrderState.Closing],
        },
        userOrders: {
          $elemMatch: {
            user: ObjectId(req.user._id),
          },
        },
      },
      {
        $pull: {
          userOrders: {
            user: ObjectId(req.user._id) as any, // TODO: figure out what this should be
          },
        },
      },
    ).then((order) => {
      if (!order) {
        throw new Error('Order is null');
      }

      req.flash('success', 'Successfully left order');
    }).catch((err) => {
      this.logger.error(`Error leaving order: ${err}`);
      req.flash('error', 'Failed to leave order; it may already be closed');
    }).then(() => next());
  }

  /**
   * @description Checks that it is valid to message for the specified
   *  order.
   * @private
   * @param {IOrder} order the order
   * @memberof OrderRouter
   */
  private checkOrderMessageTime(order: IOrder) {
    if (order.state !== OrderState.Archived) {
      return;
    }

    if (!isNowWithinHoursOf(this.messagingTime, order.closeDate)) {
      throw new Error('Outside of messaging time for order.');
    }
  }
}
