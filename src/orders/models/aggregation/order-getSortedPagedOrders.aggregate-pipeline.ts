/**
 * @description Returns the underlying aggregate pipeline for getSortedPagedOrders.
 * @export
 * @param {number} page The page number.
 * @param {number} pageSize The page size.
 * @returns The pipeline definition for getSortedPagedOrders.
 */
export function getSortedPagedOrdersPipeline(page: number, pageSize: number) {
  const $sort = {
    state: 1,
    closeDateDelta: 1,
    location: 1,
  };

  const $project = {
    purchaser: 1,
    state: 1,
    location: 1,
    description: 1,
    cost: 1,
    closeDate: 1,
    openDate: 1,
  };

  return [
    {
      $facet: {
        stats: [
          { $count: 'count' },
        ],
        orders: [
          {
            $project: {
              ...$project,
              userOrders: 1,
              closeDateDelta: {
                $abs: { $subtract: [new Date(), '$closeDate'] },
              },
            },
          },
          {
            $sort,
          },
          { $skip: pageSize * (page - 1) },
          { $limit: pageSize },
          {
            $unwind: {
              path: '$userOrders',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: 'users',
              localField: 'userOrders.user',
              foreignField: '_id',
              as: 'userOrders.user',
            },
          },
          {
            $unwind: {
              path: '$userOrders.user',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: 'users',
              localField: 'purchaser',
              foreignField: '_id',
              as: 'purchaser',
            },
          },
          {
            $unwind: {
              path: '$purchaser',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $group: {
              _id: '$_id',
              purchaser: { $first: '$purchaser' },
              state: { $first: '$state' },
              location: { $first: '$location' },
              description: { $first: '$description' },
              cost: { $first: '$cost' },
              closeDate: { $first: '$closeDate' },
              openDate: { $first: '$openDate' },
              userOrders: { $push: '$userOrders' },
              closeDateDelta: { $first: '$closeDateDelta' },
            },
          },
          {
            $project: {
              ...$project,
              closeDateDelta: 1,
              userOrders: {
                $filter: {
                  input: '$userOrders',
                  as: 'userOrder',
                  cond: {
                    $gt: ['$$userOrder._id', null],
                  },
                },
              },
            },
          },
          {
            $sort,
          },
        ],
      },
    },
    {
      $unwind: '$stats',
    },
  ];
}
