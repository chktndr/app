import {
  FacebookOAuthProvider,
  GithubOAuthProvider,
  GoogleOAuthProvider,
  IOAuthProvider,
  IOAuthProviderConfig,
} from '../auth';
import staticConfig from './staticConfig';

/**
 * @description Represents the definition of an OAuth provider.
 * @export
 * @interface IOAuthProviderDefinition
 */
export interface IOAuthProviderDefinition {
  /**
   * @description Provider constructor
   * @memberof IOAuthProviderDefinition
   */
  provider: new (config: IOAuthProviderConfig) => IOAuthProvider;

  /**
   * @description Provider name.
   * @type {string}
   * @memberof IOAuthProviderDefinition
   */
  name: string;

  /**
   * @description Provider scopes.
   * @type {string[]}
   * @memberof IOAuthProviderDefinition
   */
  scope: string[];

  /**
   * @description Provider configuration.
   * @type {*}
   * @memberof IOAuthProviderDefinition
   */
  config?: any;
}

/**
 * @description List of OAuth providers.
 */
const oAuthProviders: IOAuthProviderDefinition[] = [
  {
    provider: GoogleOAuthProvider,
    name: 'google',
    scope: ['profile', 'email'],
    config: {
      clientID: staticConfig.oAuth.google.clientID,
      clientSecret: staticConfig.oAuth.google.clientSecret,
      callbackURL: staticConfig.oAuth.google.callbackURL,
    },
  },
  {
    provider: GithubOAuthProvider,
    name: 'github',
    scope: ['read:user', 'user:email'],
    config: {
      clientID: staticConfig.oAuth.github.clientID,
      clientSecret: staticConfig.oAuth.github.clientSecret,
      callbackURL: staticConfig.oAuth.github.callbackURL,
    },
  },
  {
    provider: FacebookOAuthProvider,
    name: 'facebook',
    scope: ['email'],
    config: {
      clientID: staticConfig.oAuth.facebook.clientID,
      clientSecret: staticConfig.oAuth.facebook.clientSecret,
      callbackURL: staticConfig.oAuth.facebook.callbackURL,
    },
  },
];

export default oAuthProviders;
