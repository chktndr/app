# ChickenTenderMarketplace
[![pipeline status](https://gitlab.com/craig.t.dunford/chickentendermarketplace/badges/master/pipeline.svg)](https://gitlab.com/craig.t.dunford/chickentendermarketplace/commits/master) [![coverage report](https://gitlab.com/craig.t.dunford/chickentendermarketplace/badges/master/coverage.svg)](https://gitlab.com/craig.t.dunford/chickentendermarketplace/commits/master)

[https://www.chickentendermarketplace.ca/](https://www.chickentendermarketplace.ca/ "ChickenTenderMarketplace") - A simple site for creating group chicken orders and tracking user "coin" balances.

## Pre-requisites for running

- MongoDB 4.0+
- NodeJS 10.0+

## Running the application locally

-  Install node_modules
```
npm install
```

- Start the application
```
npm start
```

You can additionally watch for changes to source files and have the application restart:

```
npm run watch
```

## Configuring to login
In order to login with a social login provider, the `clientID` and `clientSecret`
must be set to valid values in [config.json](https://gitlab.com/craig.t.dunford/chickentendermarketplace/blob/master/src/config/static/config.json):

``` json
"oAuth": {
    "google": {
      "clientID": "test-client-id",
      "clientSecret": "test-client-secret",
      "callbackURL": "http://127.0.0.1:3000/app/auth/google/callback"
    },
    "github": {
      "clientID": "test-client-id",
      "clientSecret": "test-client-secret",
      "callbackURL": "http://127.0.0.1:3000/app/auth/github/callback"
    },
    "facebook": {
      "clientID": "test-client-id",
      "clientSecret": "test-client-secret",
      "callbackURL": "http://127.0.0.1:3000/app/auth/facebook/callback"
    }
  }
```

You can acquire credentials from the social provider of your choosing.
The production secrets are stored in docker volume on the production server.
You should avoid committing your test keys.

## Running the docker containers

In order to run the application in docker containers, you must have docker
and docker-compose installed, and the following 3 named/persistent volumes
must be created:

- chktndr-staticconfig
- chktndr-dynamiccontent
- chktndr-data

Further, your tailored `config.json` (from above) must be copied into the
chktndr-staticconfig volume. Once this is completed, you can execute:

```
docker-compose up
```

## Running the E2E tests

- Start the application

```
npm start
```

- In addition, start Selenium

```
npm run selenium
```

- Finally, run the E2E tests

```
npm run test:e2e
```

Test coverage will be captured in the `coverage` folder.

## Running the E2E tests in docker

- Navigate to `e2e/docker` in the repository and compose the containers

```
docker-compose up
```

Test coverage will be captured in the `e2e/.e2e-results/coverage` folder.

## Contributing

Have a look at the issues list and have at it!
