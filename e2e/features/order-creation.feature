Feature: Creating orders
  As a power user of ChickenTenderMarketplace
  I need to create orders
  So that we can all enjoy mouthwatering chicken

  Background: Logged in as order creator
    Given I am logged out of the system
    When I login with username "SammyOrderCreator"
    And I have navigated to the orders page

  Scenario: Successfully create new order closing in the future
    When I click on create order
    And I select order type "Tasca Piri Piri"
    And I enter the closing date "3050-09-28T11:00:00"
    And I click create
    Then I should see a successful notification that "Created new order"
    And The order list should contain an order from "Tasca Piri Piri" that is "Open" with a cost 10 and 0 order entries which closes at "Sat Sep 28 3050 11:00:00 GMT-0400 (Eastern Daylight Time)"
    And An email should be sent to "john.doe@test.local" indicating a new order from "Tasca Piri Piri" has been created which closes at "Sat Sep 28 3050 11:00:00 GMT-0400 (Eastern Daylight Time)"
    And An email should be sent to "billy.admin@test.local" indicating a new order from "Tasca Piri Piri" has been created which closes at "Sat Sep 28 3050 11:00:00 GMT-0400 (Eastern Daylight Time)"
    And An email should be sent to "sammy.ordercreator@test.local" indicating a new order from "Tasca Piri Piri" has been created which closes at "Sat Sep 28 3050 11:00:00 GMT-0400 (Eastern Daylight Time)"

  Scenario: Failed to create an order in the past
    When I click on create order
    And I select order type "Tasca Piri Piri"
    And I enter the closing date "1985-09-28T11:00:00"
    And I click create
    Then I should see an error notification that "Invalid value (closeDate)"
