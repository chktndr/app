Feature: Joining an order
  As a user of ChickenTenderMarketplace
  I want to join the orders
  So I can enjoy mouthwatering chicken

  Background: Order is created and I am logged in
    Given I am logged out of the system
    And I login with username "SammyOrderCreator"
    And I click on create order
    And I select order type "Tasca Piri Piri"
    And I enter the closing date "3050-09-28T11:00:00"
    And I click create
    And I logout
    And I login with username "JohnDoe"
    And I have navigated to the orders page

  Scenario: Successfully joining an order
    When I click join on the order from "Tasca Piri Piri" closing at "Sat Sep 28 3050 11:00:00 GMT-0400 (Eastern Daylight Time)"
    And I choose "Quarter chicken & fries" for the order option "Selection"
    And I click join
    Then I should see a successful notification that "Successfully joined order"
    And The order from "Tasca Piri Piri" closing at "Sat Sep 28 3050 11:00:00 GMT-0400 (Eastern Daylight Time)" should contain the user "John Doe" with order details "Selection: Quarter chicken & fries"
