Feature: Logging into the system
  As a user of ChickenTenderMarketplace
  I should be able to login in various ways
  So that I can eat mouthwatering chicken

  Background:
    Given I am logged out of the system

  Scenario: The homepage shows the login page for unauthenticated users
    When I navigate to the homepage
    Then I should be redirected to the login page
    And I should have a sign-in option to "Sign in with Facebook"
    And I should have a sign-in option to "Sign in with Google"
    And I should have a sign-in option to "Sign in with GitHub"

  Scenario: Login as a new user
    When I login with username "NewUser"
    Then I should see the disabled user page with the message "Oops... Your account is disabled. Please contact an administrator to have it enabled."
    And An email should be sent to "billy.admin@test.local" indicating the new user "Test User" has logged in

  Scenario: Login as an existing user
    When I login with username "JohnDoe"
    Then I should see the site navigation with "Orders" selected

  Scenario: Unauthenticated user is redirected to login
    When I navigate to the "orders" page
    Then I should be redirected to the login page
    And I should have a sign-in option to "Sign in with Facebook"
    And I should have a sign-in option to "Sign in with Google"
    And I should have a sign-in option to "Sign in with GitHub"
