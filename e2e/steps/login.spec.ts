/* tslint:disable:only-arrow-functions no-unused-expression */
import { expect } from 'chai';
import { Given, Then, When } from 'cucumber';
import { browser } from 'protractor';
import { DisabledUserPage } from '../pages/disabledUser.po';
import { LoginPage } from '../pages/login.po';
import { PageWithNavigation } from '../pages/navigation.po';
import { TestMailPage } from '../pages/testMail.po';
import '../support/world';

Given('I am logged out of the system', async function() {
  await browser.get('/app/auth/logout');
  this.page = new LoginPage();
});

When('I navigate to the homepage', function() {
  return browser.get('/');
});

When('I login with username {string}', async function(username: string) {
  this.page = await this.getPage<LoginPage>().login(username);
});

When('I logout', async function() {
  this.page = await this.getPage<PageWithNavigation>().clickLogout();
});

Then('I should have a sign-in option to {string}', async function(option: string) {
  const page = this.getPage<LoginPage>();
  const options = await page.getSignInOptions();
  expect(options.some((opt) => opt === option)).to.be.true;
});

Then('I should see the disabled user page with the message {string}', async function(message: string) {
  const page = this.getPage<DisabledUserPage>();
  await expect(page.getMessage()).to.eventually
    .equal(message);
});

Then('I should be redirected to the login page', async function() {
  const page = new LoginPage();
  await expect(page.getTitle()).to.eventually.equal('Chicken Tender Marketplace');
  await expect(page.getUrl()).to.eventually.equal('/app/auth/login');

  this.page = page;
});

Then(
  'An email should be sent to {string} indicating the new user {string} has logged in',
  async function(to: string, newUser: string) {
    const mail = await TestMailPage.navigateToLatest();
    await expect(mail.getSubject()).to.eventually.equal(`A new user has logged in: ${newUser}`);
    await expect(mail.getToAddresses()).to.eventually.include.members([to]);
  });
