/* tslint:disable:only-arrow-functions */
import { expect } from 'chai';
import { Then, When } from 'cucumber';
import { browser } from 'protractor';
import { ConfirmationPage } from '../pages/confirmation.po';
import { PageWithNavigation } from '../pages/navigation.po';
import '../support/world';

When('I navigate to the {string} page', function(page: string) {
  return browser.get(`/app/${page}`);
});

When('I select {string} on the confirmation page', async function(selection: string) {
  this.page = await this.getPage<ConfirmationPage>().makeSelection(selection);
});

Then('I should see the site navigation with {string} selected', async function(nav: string) {
  await expect(this.getPage<PageWithNavigation>().getSelectedNavigation()).to.eventually.equal(nav);
});

Then('I should see a successful notification that {string}', async function(message: string) {
  const notifications = await this.page.getSuccessNotifications();
  expect(notifications).to.include.members([message]);
});

Then('I should see an error notification that {string}', async function(message: string) {
  const notifications = await this.page.getErrorNotifications();
  expect(notifications).to.include.members([message]);
});
