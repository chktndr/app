/* tslint:disable:only-arrow-functions no-unused-expression */
import { expect } from 'chai';
import { Given, Then, When } from 'cucumber';
import moment = require('moment');
import { CreateOrderPage } from '../pages/createOrder.po';
import { JoinOrderPage } from '../pages/joinOrder.po';
import { OrdersPage } from '../pages/orders.po';
import { TestMailPage } from '../pages/testMail.po';
import '../support/world';

Given('I have navigated to the orders page', async function() {
  this.page = await OrdersPage.navigateTo();
});

When('I click on create order', async function() {
  this.page = await this.getPage<OrdersPage>().clickCreateOrder();
});

When('I select order type {string}', function(type: string) {
  return this.getPage<CreateOrderPage>().selectType(type);
});

When('I enter the closing date {string}', async function(time: string) {
  await this.getPage<CreateOrderPage>().enterClosingDate(moment(time));
});

When('I click create', async function() {
  this.page = await this.getPage<CreateOrderPage>().clickCreateOrder();
});

When('I click join on the order from {string} closing at {string}', async function(from: string, closes: string) {
  const order = await this.getPage<OrdersPage>().getOrder(from, closes);
  this.page = await order.join();
});

When('I choose {string} for the order option {string}', function(selection: string, option: string) {
  return this.getPage<JoinOrderPage>().makeSelection(option, selection);
});

When('I click join', async function() {
  this.page = await this.getPage<JoinOrderPage>().clickJoin();
});

When('I click leave on the order from {string} closing at {string}', async function(from: string, closes: string) {
  const order = await this.getPage<OrdersPage>().getOrder(from, closes);
  this.page = await order.leave();
});

Then(
  'An email should be sent to {string} indicating a new order from {string} has been created which closes at {string}',
  async function(to: string, orderLocation: string, closes: string) {
    const toList = to.split(';');
    const email = await TestMailPage.navigateToLatest();

    await expect(email.getToAddresses()).to.eventually.include.members(toList);
    await expect(email.getSubject()).to.eventually
      .equal(`New order created from '${orderLocation}'; closing @ ${closes}`);
  });

Then(
  // tslint:disable-next-line:max-line-length
  'The order list should contain an order from {string} that is {string} with a cost {int} and {int} order entries which closes at {string}',
  async function(from: string, state: string, cost: number, numOrders: number, closes: string) {
    const order = await this.getPage<OrdersPage>().getOrder(from, closes);
    expect(order.cost).to.equal(cost);
    expect(order.numOrders).to.equal(numOrders);
    expect(order.state).to.equal(state);
  });

Then(
  'The order from {string} closing at {string} should contain the user {string} with order details {string}',
  async function(from: string, closes: string, user: string, details: string) {
    const order = await this.getPage<OrdersPage>().getOrder(from, closes);

    expect(order.entries.find((entry) => {
      return entry.user === user && entry.details.find((entryDetails) => {
        return entryDetails === details;
      }) !== undefined;
    })).not.to.be.undefined;
  });

Then(
  'The order from {string} closing at {string} should not contain the user {string}',
  async function(from: string, closes: string, user: string) {
    const order = await this.getPage<OrdersPage>().getOrder(from, closes);

    expect(order.entries.find((entry) => {
      return entry.user === user;
    })).to.be.undefined;
  });
