import config from '../../../src/config/config.common';
import { TestOAuthProvider } from '../auth';
import { TestDustSmtpMailer } from '../mailing/testDustSmtpMailer';

const appHost = process.env.APP_HOST ? process.env.APP_HOST : '127.0.0.1:3000';

const testConfig = config;

const testOAuthProvider = {
  provider: TestOAuthProvider,
  name: 'test',
  scope: ['profile', 'email'],
};

testConfig.routes.forEach((route) => {
  if (route && route.config) {
    if (route.config.providers) {
      route.config.providers.push(testOAuthProvider);
    }
    if (route.config.oAuthProviders) {
      route.config.oAuthProviders.push(testOAuthProvider);
    }
    if (route.config.mailer) {
      route.config.mailer = new TestDustSmtpMailer({
        userName: '',
        password: '',
        siteRoot: `http://${appHost}`,
        smtpFrom: '',
        smtpService: '',
        templateDirectory: `${__dirname}/../../../src/mailing/templates/`,
      });
    }
  }
});

export default testConfig;
