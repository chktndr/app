import { Strategy } from 'passport';
import * as UrlStrategy from 'url-strategy';
import { IOAuthProvider, IOAuthUser, ValidatorFunction } from '../../../../src/auth';

/**
 * @description Simple authentication provider for testing puposes.
 * @export
 * @class TestOAuthProvider
 * @implements {IOAuthProvider}
 */
export class TestOAuthProvider implements IOAuthProvider {
  /**
   * @description Returns the authentication strategy.
   * @param {ValidatorFunction} validator Validator function.
   * @returns {Strategy} Passport strategy.
   * @memberof TestOAuthProvider
   */
  public getStrategy(validator: ValidatorFunction): Strategy {
    return new UrlStrategy({
      failRedirect: '/',
      varName: 'id',
    }, (id: any, done: (err: any, user?: any, options?: { message: string; } | undefined) => void) => {
      const profile: IOAuthUser = {
        id,
        firstName: 'Test',
        lastName: 'User',
        email: 'user@test.local',
      };

      validator('', '', profile, done);
    });
  }

  /**
   * @description Parses the retrieved profile to standard form.
   * @param {*} profile The provider specific profile object.
   * @returns {IOAuthUser} The parsed profile object.
   * @memberof TestOAuthProvider
   */
  public parseProfile(profile: any): IOAuthUser {
    return profile;
  }

}
