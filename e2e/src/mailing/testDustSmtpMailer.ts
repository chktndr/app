import { createTestAccount, createTransport, getTestMessageUrl, TestAccount } from 'nodemailer';
import { DustSmtpMailer, IDustSmtpMailerConfig } from '../../../src/mailing';
import { TestMailMessage } from './models/testMailMessage';

export class TestDustSmtpMailer extends DustSmtpMailer {

  /**
   * @description Test SMTP account settings.
   * @private
   * @type {TestAccount}
   * @memberof TestDustSmtpMailer
   */
  private testAccountPromise: Promise<TestAccount> = this.getTestAccount();

  /**
   * Creates an instance of TestDustSmtpMailer.
   * @param {IDustSmtpMailerConfig} config Mailer configuration.
   * @memberof DustSmtpMailer
   */
  constructor(
    config: IDustSmtpMailerConfig,
  ) {
    super(config);
  }

  /**
   * @description Sends a raw email.
   * @protected
   * @param {string} body The raw body.
   * @param {string} subject The raw subject.
   * @param {string} to The raw to line.
   * @param {string} cc The raw cc line.
   * @returns
   * @memberof DustSmtpMailer
   */
  protected async send(body: string, subject: string, to: string, cc: string) {
    const testAccount = await this.testAccountPromise;

    const transport = createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false,
      tls: {
        rejectUnauthorized: false,
      },
      auth: {
        user: testAccount.user,
        pass: testAccount.pass,
      },
    });

    return transport.sendMail({
      from: 'test@test.local',
      to,
      cc,
      subject,
      html: body,
    }).then((info) => {
      const url = getTestMessageUrl(info) as string;

      // tslint:disable-next-line:no-console
      console.log(`THE TEST MESSAGE URL IS: ${url}`);

      const message = new TestMailMessage({
        url,
        created: new Date(),
      });

      return message.save();
    });
  }

  /**
   * @description Creates a test account for sending mail.
   * @private
   * @returns A promise resolved with test account details.
   * @memberof TestDustSmtpMailer
   */
  private async getTestAccount() {
    return createTestAccount();
  }
}
