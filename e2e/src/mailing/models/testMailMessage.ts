import { Document, model, Schema } from 'mongoose';

/**
 * @description Mongoose schema for a test mail message.
 */
const testMailMessageSchema = new Schema({
  url: {
    required: true,
    type: String,
  },
  created: {
    required: true,
    type: Date,
  },
});

/**
 * @description Represents a test mail message.
 * @export
 * @interface IOrder
 * @extends {Document}
 */
export interface ITestMailMessage extends Document {
  /**
   * @description The url of the test mail message.
   * @type {string}
   * @memberof ITestMailMessage
   */
  url: string;

  /**
   * @description The message creation time.
   * @type {string}
   * @memberof ITestMailMessage
   */
  created: Date;
}

const TestMailMessage = model<ITestMailMessage>('TestMailMessage', testMailMessageSchema);

export { TestMailMessage };
