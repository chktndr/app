import { browser, By, element } from 'protractor';
import { ConfirmationPage } from './confirmation.po';
import { CreateOrderPage } from './createOrder.po';
import { JoinOrderPage } from './joinOrder.po';
import { PageWithNavigation } from './navigation.po';

/**
 * @description Represents the orders page.
 * @export
 * @class OrdersPage
 * @extends {PageWithNavigation}
 */
export class OrdersPage extends PageWithNavigation {

  /**
   * @description Navigates to the orders page.
   * @static
   * @returns A new orders page.
   * @memberof OrdersPage
   */
  public static async navigateTo() {
    await browser.get('/app/orders');
    return new OrdersPage();
  }

  /**
   * @description Clicks the create order button
   * @returns The Create Order page.
   * @memberof OrdersPage
   */
  public async clickCreateOrder() {
    await element(By.cssContainingText('a', 'Create Order')).click();
    return new CreateOrderPage();
  }

  /**
   * @description Gets the list of orders currently in the view.
   * @returns The list of orders in the view.
   * @memberof OrdersPage
   */
  public async getOrders() {
    return element.all(By.css('li.list-group-card')).map<IOrderView>(async (orderCard) => {
      if (!orderCard) {
        throw new Error('Order card is undefined.');
      }

      const order: Partial<IOrderView> = {};

      order.from = (await orderCard.element(By.css('h3 span')).getText()).trim();
      order.state = await orderCard.element(By.css('h3 small')).getText();

      const costElement = orderCard.element(By.cssContainingText('span', 'Cost: '));
      order.cost =
        parseInt(await costElement.element(By.xpath('following-sibling::span[1]')).getText(), 10);

      const closes = await orderCard.element(By.cssContainingText('span', /^Close[sd]: /)).getText();
      order.closes = closes.substr(8);

      const numOrders = await orderCard.element(By.cssContainingText('h4', /^Orders \([0-9]+\)/)).getText();

      const matches = /^Orders \(([0-9]+)\)/.exec(numOrders);
      if (!matches) {
        throw new Error('Unable to parse order count.');
      }

      order.numOrders = parseInt(matches[1], 10);

      order.entries = await orderCard.all(By.css('div.well table tbody tr')).map<IOrderEntry>(async (entryRow) => {
        if (!entryRow) {
          throw new Error('Order entry row is undefined.');
        }

        const user = await entryRow.element(By.css('td > span:nth-child(2)')).getText();

        const details = await entryRow.all(By.css('td > ul > li')).map<string>((orderDetail) => {
          if (!orderDetail) {
            throw new Error('Order detail is undefined.');
          }

          return orderDetail.getText();
        });

        return {
          user,
          details,
        };

      });

      order.join = async () => {
        await orderCard.element(By.cssContainingText('a.btn-info', 'Join')).click();
        return new JoinOrderPage();
      };

      order.leave = async () => {
        await orderCard.element(By.cssContainingText('a.btn-info', 'Leave')).click();
        return new ConfirmationPage(this);
      };

      return order;
    });
  }

  /**
   * @description Finds an order from a location that closes at a specific time.
   * @param {string} from The order location.
   * @param {string} closes When the order closes.
   * @returns The order view.
   * @memberof OrdersPage
   */
  public async getOrder(from: string, closes: string) {
    const order = (await this.getOrders()).find((possibleOrder) => {
      return possibleOrder.from === from && possibleOrder.closes === closes;
    });

    if (!order) {
      throw new Error('Order not found.');
    }

    return order;
  }
}

/**
 * @description Represents the view of an order.
 * @export
 * @interface IOrderView
 */
export interface IOrderView {
  from: string;
  state: string;
  closes: string;
  cost: number;
  numOrders: number;

  entries: IOrderEntry[];

  join: () => Promise<JoinOrderPage>;
  leave: () => Promise<ConfirmationPage>;
}

/**
 * @description Represents the entries in an order.
 * @export
 * @interface IOrderEntry
 */
export interface IOrderEntry {
  user: string;
  details: string[];
}
