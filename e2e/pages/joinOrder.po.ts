import { By, element } from 'protractor';
import { PageWithNavigation } from './navigation.po';
import { OrdersPage } from './orders.po';

/**
 * @description The join order page.
 * @export
 * @class JoinOrderPage
 * @extends {PageWithNavigation}
 */
export class JoinOrderPage extends PageWithNavigation {

  /**
   * @description Makes a selection from the order's options
   * @param {string} name The name of the selection list.
   * @param {string} value The value of the selection.
   * @returns A promise resolved when the item has been selected.
   * @memberof JoinOrderPage
   */
  public makeSelection(name: string, value: string) {
    return element(By.css(`[name="${name}"]`))
      .element(By.cssContainingText('option', value)).click();
  }

  /**
   * @description Clicks the join button.
   * @returns A new {@see OrdersPage}.
   * @memberof JoinOrderPage
   */
  public async clickJoin() {
    await element(By.cssContainingText('button', 'Join')).click();
    return new OrdersPage();
  }
}
