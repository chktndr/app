import { Moment } from 'moment';
import { By, element, Key } from 'protractor';
import { PageWithNavigation } from './navigation.po';
import { OrdersPage } from './orders.po';

/**
 * @description The create order page.
 * @export
 * @class CreateOrderPage
 * @extends {PageWithNavigation}
 */
export class CreateOrderPage extends PageWithNavigation {

  /**
   * @description Selects the order type.
   * @param {string} type The type of order to select.
   * @returns A promise resolved when the type has been selected.
   * @memberof CreateOrderPage
   */
  public selectType(type: string) {
    return element(By.css('#type')).element(By.cssContainingText('option', type)).click();
  }

  /**
   * @description Enters the order closing date
   * @param {Date} date The closing date.
   * @memberof CreateOrderPage
   */
  public async enterClosingDate(date: Moment) {
    const dateStr = date.format('MMDDYYYY');
    const timeStr = date.format('hhmma');
    const dateElement = element(By.css('#closeDateLocal'));
    await dateElement.sendKeys(dateStr);
    await dateElement.sendKeys(Key.TAB);
    await dateElement.sendKeys(timeStr);
  }

  /**
   * @description Clicks the create button.
   * @returns A new orders page.
   * @memberof CreateOrderPage
   */
  public async clickCreateOrder() {
    await element(By.css('form button[type="submit"]')).click();
    return new OrdersPage();
  }
}
