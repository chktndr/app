import { browser, By, element, ExpectedConditions } from 'protractor';
import { BasePage } from './base.po';
import { DisabledUserPage } from './disabledUser.po';
import { OrdersPage } from './orders.po';

/**
 * @description Login Page Object
 * @export
 * @class LoginPage
 */
export class LoginPage extends BasePage {

  /**
   * @description Navigates to the login page.
   * @returns Promise resolved when navigation is complete.
   * @memberof LoginPage
   */
  public navigateTo() {
    return browser.get('/app/auth/login');
  }

  /**
   * @description Get the list of sign in options.
   * @returns Promise resolved with the list of sign options.
   * @memberof LoginPage
   */
  public async getSignInOptions() {
    await browser.wait(ExpectedConditions.presenceOf(element(By.css('.login-button-row'))));
    return element.all(By.css('.login-button-row a:last-child'))
      .map<string>((el) => el ? el.getText() : '');
  }

  public async login(username: string) {
    await browser.get(`/app/auth/test?id=${username}`);
    const url = await this.getUrl();
    if (url === '/app/auth/disabled') {
      return new DisabledUserPage();
    }

    return new OrdersPage();
  }
}
