import { browser, By, element } from 'protractor';

/**
 * @description Base page object all other pages extend from.
 * @export
 * @class BasePage
 */
export class BasePage {

  /**
   * @description Retrieves the page's title.
   * @returns The page's title.
   * @memberof BasePage
   */
  public getTitle() {
    return browser.getTitle();
  }

  /**
   * @description Retrieves the current URL relative to the application root.
   * @returns The current URL relative to the application root.
   * @memberof BasePage
   */
  public getUrl() {
    return browser.getCurrentUrl()
      .then((url) => url.substring(browser.baseUrl.length));
  }

  /**
   * @description Gets all of the success notifications on the page.
   * @returns An array of all the success notification text.
   * @memberof BasePage
   */
  public getSuccessNotifications() {
    return element.all(By.css('div.alert-success > span')).map<string>((el) => el ? el.getText() : '');
  }

  /**
   * @description Gets all of the error notifications on the page.
   * @returns An array of all the error notification text.
   * @memberof BasePage
   */
  public getErrorNotifications() {
    return element.all(By.css('div.alert-danger > span')).map<string>((el) => el ? el.getText() : '');
  }
}
