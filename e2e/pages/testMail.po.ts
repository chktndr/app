import { browser, By, element, ExpectedConditions } from 'protractor';
import { TestMailMessage } from '../src/mailing/models/testMailMessage';
import { BasePage } from './base.po';

/**
 * @description Represents the a test e-mail page.
 * @export
 * @class OrdersPage
 * @extends {BasePage}
 */
export class TestMailPage extends BasePage {

  /**
   * @description Navigates to the latest message.
   * @static
   * @returns A promise resolved with the TestMailPage object.
   * @memberof TestMailPage
   */
  public static async navigateToLatest() {
    const message = await TestMailMessage.findOne({}, {}, { sort: { created: -1 } }).exec();
    if (!message) {
      throw new Error('No test mail message found.');
    }

    await browser.get(message.url);
    await browser.wait(ExpectedConditions.presenceOf(element(By.css('#message-header'))));
    return new TestMailPage();
  }

  /**
   * @description Gets the to addresses of the mail.
   * @returns The to addresses.
   * @memberof TestMailPage
   */
  public async getToAddresses() {
    const addresses = await element.all(By.css('#message-header > div:nth-child(3) a.mp_address_email'))
      .map<string>((el) => el ? el.getAttribute('href') : '');

    return addresses.map((address) => address.replace('mailto:', ''));
  }

  /**
   * @description Gets the email subject.
   * @returns The email subject.
   * @memberof TestMailPage
   */
  public getSubject() {
    return element(By.css('#message-header > div:nth-child(1) > span')).getText();
  }
}
