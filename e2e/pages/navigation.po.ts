import { browser, By, element, ExpectedConditions } from 'protractor';
import { BasePage } from './base.po';
import { LoginPage } from './login.po';

/**
 * @description Base page object for pages with navigation
 * @export
 * @class PageWithNavigation
 * @extends {BasePage}
 */
export class PageWithNavigation extends BasePage {

  /**
   * @description Retrieves the selected navigation item.
   * @returns The selected navigation text.
   * @memberof PageWithNavigation
   */
  public async getSelectedNavigation() {
    await browser.wait(ExpectedConditions.presenceOf(element(By.css('.navbar-nav'))));
    return element(By.css('.navbar-nav li.active a')).getText();
  }

  public async clickLogout() {
    await browser.wait(ExpectedConditions.presenceOf(element(By.css('.navbar-nav'))));
    await element(By.css('ul.navbar-right a.dropdown-toggle')).click();
    await element(By.cssContainingText('a', 'Logout')).click();
    return new LoginPage();
  }
}
