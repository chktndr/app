import { browser, By, element, ExpectedConditions } from 'protractor';
import { BasePage } from './base.po';

/**
 * @description Represents the disabled user page
 * @export
 * @class DisabledUserPage
 */
export class DisabledUserPage extends BasePage {

  /**
   * @description Gets the presented message text.
   * @returns The message text.
   * @memberof DisabledUserPage
   */
  public getMessage() {
    browser.wait(ExpectedConditions.presenceOf(element(By.css('h3'))));
    return element(By.css('h3')).getText();
  }

}
