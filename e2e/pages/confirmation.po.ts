import { By, element } from 'protractor';
import { BasePage } from './base.po';
import { PageWithNavigation } from './navigation.po';

/**
 * @description Page with a simple confirmation.
 * @export
 * @class ConfirmationPage
 * @extends {PageWithNavigation}
 */
export class ConfirmationPage extends PageWithNavigation {

  /**
   * Creates an instance of ConfirmationPage.
   * @param {BasePage} previousPage The previous page object.
   * @memberof ConfirmationPage
   */
  constructor(private previousPage: BasePage) {
    super();
  }

  /**
   * @description Makes a confirmation selection.
   * @param {string} selection The selection option to make.
   * @returns The previous page object.
   * @memberof ConfirmationPage
   */
  public async makeSelection(selection: string) {
    await element(By.cssContainingText('div.btngroup a.btn', selection)).click();
    return this.previousPage;
  }
}
