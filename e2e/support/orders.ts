import { Before } from 'cucumber';
import { Order } from '../../src/orders';

Before(async () => {
  await Order.deleteMany({});
});
