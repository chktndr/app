import { BeforeAll } from 'cucumber';
import { connectToMongo } from '../../src/utility';

BeforeAll(() => {
  const mongoConnectionOptions = {
    useNewUrlParser: true,
    autoReconnect: true,
    reconnectTries: Number.MAX_SAFE_INTEGER,
    reconnectInterval: 5000,
  };

  const dbUri = process.env.DB_HOST
    ? `mongodb://${process.env.DB_HOST}/chickentender`
    : 'mongodb://127.0.0.1/chickentender';

  return connectToMongo(dbUri, mongoConnectionOptions);
});
