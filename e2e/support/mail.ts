import { Before } from 'cucumber';
import { TestMailMessage } from '../src/mailing/models/testMailMessage';

Before(async () => {
  await TestMailMessage.deleteMany({});
});
