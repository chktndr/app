import { setWorldConstructor, World } from 'cucumber';
import { BasePage } from '../pages/base.po';

declare module 'cucumber' {
  // tslint:disable-next-line: interface-name
  export interface World {
    /**
     * @description Page object
     * @type {BasePage}
     * @memberof World
     */
    page: BasePage;

    /**
     * @description Retrieves the current page object.
     * @template T The type of the page object.
     * @returns The current page object.
     * @memberof World
     */
    getPage<T extends BasePage>(): T;

    /**
     * @description Test specific data dictionary.
     */
    [k: string]: any;
  }
}

class ChickenTenderWorld implements World {
  /**
   * @description Page object
   * @type {BasePage}
   * @memberof ChickenTenderWorld
   */
  public page: any;

  /**
   * @description Retrieves the current page object.
   * @template T The type of the page object.
   * @returns The current page object.
   * @memberof ChickenTenderWorld
   */
  public getPage<T extends BasePage>() {
    return this.page as T;
  }

  /**
   * @description Test specific data dictionary.
   */
  [k: string]: any;
}

setWorldConstructor(ChickenTenderWorld);
