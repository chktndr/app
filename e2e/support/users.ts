import { Before } from 'cucumber';
import { User } from '../../src/users';

Before(async () => {
  await User.deleteMany({});

  await new User({
    oAuthIds: [
      {
        id: 'JohnDoe',
        authType: 'test',
      },
    ],
    firstName: 'John',
    lastName: 'Doe',
    enabled: true,
    activated: true,
    email: 'john.doe@test.local',
  }).save();

  await new User({
    oAuthIds: [
      {
        id: 'BillyAdmin',
        authType: 'test',
      },
    ],
    firstName: 'Billy',
    lastName: 'Admin',
    enabled: true,
    activated: true,
    roles: ['admin'],
    email: 'billy.admin@test.local',
  }).save();

  await new User({
    oAuthIds: [
      {
        id: 'SammyOrderCreator',
        authType: 'test',
      },
    ],
    firstName: 'Sammy',
    lastName: 'OrderCreator',
    enabled: true,
    activated: true,
    roles: ['order-creator'],
    email: 'sammy.ordercreator@test.local',
  }).save();
});
