const process = require('process');
const seleniumHost = process.env.SELENIUM_HOST ? process.env.SELENIUM_HOST : '127.0.0.1:4444';
const appHost = process.env.APP_HOST ? process.env.APP_HOST : '127.0.0.1:3000';

exports.config = {
  seleniumAddress: `http://${seleniumHost}/wd/hub`,
  getPageTimeout: 60000,
  allScriptsTimeout: 500000,
  framework: 'custom',
  // path relative to the current config file
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  capabilities: {
    'browserName': 'chrome',
    chromeOptions: {
      args: ["--headless", "--disable-gpu", "--window-size=1024,768", "--no-sandbox"]
    }
  },
  specs: [
    'features/**/*.feature',
  ],
  baseUrl: `http://${appHost}`,
  cucumberOpts: {
    compiler: 'ts:ts-node/register',
    require: ['steps/**/*.spec.ts', 'support/**/*.ts'],
    format: ['summary', 'json:.cucumberjs/results.json']
  },

  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, '../tsconfig.json')
    });
    browser.ignoreSynchronization = true;
  }
}
