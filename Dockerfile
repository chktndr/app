FROM node:lts-alpine

WORKDIR /app
ADD src ./src/
ADD *.json ./

RUN npm install --production
RUN apk add --no-cache tzdata

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait

VOLUME [ "/app/src/config/static" ]
VOLUME [ "/app/src/dynamicContent" ]

EXPOSE 3000

CMD /wait && node -r ts-node/register/transpile-only src/index.ts
